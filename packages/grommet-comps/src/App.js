import React from "react";
import { Box, Grommet } from "grommet";
import GromTable from "./Table";
import "./App.css";

const theme = {
  global: {
    font: {
      family: "Roboto",
      size: "18px",
      height: "20px",
    },
  },
};

const AppBar = (props) => (
  <Box
    tag="header"
    direction="row"
    align="center"
    justify="between"
    background="brand"
    pad={{ left: "medium", right: "small", vertical: "small" }}
    elevation="medium"
    style={{ zIndex: "1" }}
    {...props}
  />
);

const App = () => (
  <Grommet theme={theme}>
    <AppBar>Hello Grommet!</AppBar>
    <GromTable />
  </Grommet>
);

export default App;
